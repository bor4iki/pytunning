import socket
import struct
import time
from matplotlib import pyplot as plt

TAB = 10
ADRESS = '192.168.1.21'
PORT = 40230

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.connect((ADRESS, PORT))


def send_msg(sock):
    # Prefix each message with a 4-byte length (network byte order)
    # msg = struct.pack('>I', len(msg)) + msgSIZE = 20
    SIZE = 20
    WHAT = 6
    one = 4
    second = 1701208
    third = 18773440
    four = 0
    message = struct.pack('<iiiiii', SIZE, WHAT, one, second, third, four)
    time.sleep(2)
    sock.sendall(message)


def recv_msg(sock):
    # Read message length and unpack it into an integer
    raw_msglen = recvall(sock, 8)
    if not raw_msglen:
        return None
    Temper = struct.unpack('<f', raw_msglen[:4])
    print(f"Температура {Temper[0]}")
    tochki = struct.unpack('<i', raw_msglen[4:8])
    print(f"Количечтво точек {tochki[0]}")

    raw_msglen = recvall(sock, 24000)
    if not raw_msglen:
        return None
    sdvig = struct.unpack(f'{12000}h', raw_msglen)
    plt.plot(sdvig)
    plt.show()
    # print(f"Сдвиг {sdvig}")
    # return recvall(sock, len(raw_msglen))


def recvall(sock, n):
    # Helper function to recv n bytes or return None if EOF is hit
    data = bytearray()
    while len(data) < n:
        packet = sock.recv(n - len(data))
        if not packet:
            return None
        data.extend(packet)
    return data


i = 0
for i in range(10):
    send_msg(sock=s)
    recv_msg(sock=s)
    i += 1
# print(f'Отправка: {WHAT}')
# message = struct.pack('<iiiiii', SIZE, WHAT, one, second, third, four)
# s.send(message)
#
# msg = s.recv(4)
# # print(msg)
# msg = struct.unpack('<f', msg)
# print(msg)
#
# msg = s.recv(4)
# # print(msg)
# tochki = struct.unpack('<i', msg)
# print(tochki[0])
# tochki_ = tochki[0] * 4
#
# time.sleep(0.5)
# msg = s.recv(24000)
# print(len(msg))
# interferogram = struct.unpack(f'{12000}h', msg)
# # msg = struct.unpack('<i', msg)
# print(interferogram)

# full_mess = b''
# new_msg = True
# while True:
# msg = s.recv(4)
# if new_msg:
#     print(f'New message length: {msg[:TAB]}')
#     msglen = int(msg[:TAB])
#     new_msg = False
# full_mess += msg
# if len(full_mess)-TAB == msglen:
#     print("Full message recvd")
#     print(full_mess[TAB:])
#     d = pickle.loads(full_mess[TAB:])
#     print(d)
#     new_msg = True
#     full_mess = b''
# print(full_mess)
