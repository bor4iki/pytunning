import socket
import struct
import configparser


class ClientBomTuning:
    def __init__(self):
        self.settings = configparser.ConfigParser()
        self.settings.read('settings.ini')
        ADRESS = self.settings.get("Connections", "adress")
        PORT = int(self.settings.get("Connections", "port"))
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((ADRESS, PORT))

    def send_msg(self, WHAT: int):
        SIZE = 20
        one = 4
        second = 1701208
        third = 18773440
        four = 0
        message = struct.pack('<iiiiii', SIZE, WHAT, one, second, third, four)
        self.s.sendall(message)

    def recv_msg(self, WHAT: int, channel: int = None):
        how_much_bytes: int = 8 + 32 * 5 * 24000
        raw_msglen = self.recvall(self.s, how_much_bytes if WHAT == 5 else 24008)
        if not raw_msglen:
            return None
        temperature = struct.unpack('<f', raw_msglen[:4])
        quantity = struct.unpack('<i', raw_msglen[4:8])
        if WHAT == 5:
            step: int = 8 + channel * 120000
            points = struct.unpack(f'{quantity[0]}h',
                                   raw_msglen[step:step + quantity[0]*2])
        else:
            points = struct.unpack(f'{quantity[0] if quantity[0] < 12000 else 12000}h',
                                   raw_msglen[8:(quantity[0] * 2 + 8) if quantity[0] < 12000 else 24008])
        return temperature, quantity, points[2:]

    @staticmethod
    def recvall(sock, n):
        data = bytearray()
        while len(data) < n:
            packet = sock.recv(n - len(data))
            if not packet:
                return None
            data.extend(packet)
        return data


if __name__ == '__main__':
    client = ClientBomTuning()
    client.send_msg(5)
    Temper, tochki, sdvig = client.recv_msg(5, 1)
    print(f'Температура прибора: {Temper[0]}')
    print(f'Количество точек: {tochki[0]}')
    # print(f'Свигограмма: {sdvig}')
