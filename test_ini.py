import configparser


def createConfig(file: str):
    config = configparser.ConfigParser()
    config.add_section("Connections")
    config.set("Connections", 'adress', '192.168.1.21')
    config.set("Connections", 'port', '40230')

    with open(file, 'w') as f:
        config.write(f)


if __name__ == '__main__':
    file = 'settings.ini'
    createConfig(file)

    settings = configparser.ConfigParser()
    settings.read('settings.ini')
    ADRESS = settings.get("Connections", "adress")
    PORT = int(settings.get("Connections", "port"))
    print(ADRESS, type(ADRESS))
    print(PORT, type(PORT))
