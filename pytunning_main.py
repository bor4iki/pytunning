from PyQt5 import QtCore
from PyQt5.QtCore import QThread
from PyQt5.QtWidgets import QMainWindow, QApplication
import pyqtgraph as pg

from pytunning import Ui_MainWindow  # импорт нашего сгенерированного файла
from client_BOM import ClientBomTuning
import sys


class StartTCP(QThread):
    GRAPH = QtCore.pyqtSignal(tuple)

    def __init__(self, WHAT: int, channel: int = None, parent=None):
        super(StartTCP, self).__init__(parent)
        self.WHAT = WHAT
        self.channel = channel
        self.TRIGGER = True

    def run(self):
        print('[USER START THREAD]')
        client = ClientBomTuning()
        while self.TRIGGER:
            client.send_msg(WHAT=self.WHAT)
            Temper, tochki, period = client.recv_msg(WHAT=self.WHAT,
                                                     channel=self.channel if self.channel else None)
            self.GRAPH.emit((Temper, tochki, period))
            if not self.stopped():
                break
        print('[USER STOPPED THREAD]')

    def stopped(self):
        return self.TRIGGER


class PyTunning(QMainWindow):
    def __init__(self):
        super(PyTunning, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.StartTCP_instance = ''

        self.set_new_graph_wid()
        self.pen = pg.mkPen(color=(255, 0, 0))
        self.graph = self.ui.graphicsView
        self.graph.setBackground('w')

        self.ui.pushButton.clicked.connect(self.start)
        self.ui.pushButton_2.clicked.connect(self.exit_prog)

    def set_new_graph_wid(self):
        self.ui.graphicsView = pg.PlotWidget(self.ui.centralwidget)
        self.ui.graphicsView.setGeometry(QtCore.QRect(10, 190, 1061, 401))
        self.ui.graphicsView.setObjectName("graphicsView")

    def start(self):
        if self.ui.pushButton.text() == 'Старт':
            if self.ui.radioButton_2.isChecked():
                self.StartTCP_instance = StartTCP(WHAT=6)
            elif self.ui.radioButton_3.isChecked():
                self.StartTCP_instance = StartTCP(WHAT=7)
            elif self.ui.radioButton.isChecked():
                channel = self.ui.lineEdit_7.text()
                self.StartTCP_instance = StartTCP(WHAT=5, channel=int(channel))
            self.StartTCP_instance.GRAPH.connect(self.graph_on)
            self.StartTCP_instance.start()
            self.ui.pushButton.setText('Стоп')
            self.StartTCP_instance.TRIGGER = True
        elif self.ui.pushButton.text() == 'Стоп':
            self.ui.pushButton.setText('Старт')
            self.StartTCP_instance.TRIGGER = False
            # self.StartTCP_instance.quit()
            # self.StartTCP_instance.exit()

    def graph_on(self, value):
        temper = str(value[0][0])
        self.ui.lineEdit.setText(temper)
        quantity = str(value[1][0])
        self.ui.lineEdit_2.setText(quantity)

        self.graph.clear()
        graph_plot = value[2]
        self.ui.lab_Y_max.setText(str(max(graph_plot)))
        self.ui.lineEdit_6.setText(str(max(graph_plot)))
        mean = (max(graph_plot) + min(graph_plot)) / 2
        self.ui.lab_Y_mean.setText(str(round(mean)))
        self.ui.lab_Y_min.setText(str(min(graph_plot)))
        self.ui.lineEdit_4.setText(str(min(graph_plot)))
        self.graph.plot(graph_plot, pen=self.pen)
        max_x, min_x = self.count_min_or_max_points(graph_plot)
        self.ui.lineEdit_3.setText(str(min_x))
        self.ui.lineEdit_5.setText(str(max_x))
        if self.ui.radioButton.isChecked():
            self.ui.lineEdit_13.setText(str(max_x))
            self.ui.lineEdit_14.setText(str(int(int(quantity) - max_x)))
        else:
            self.ui.lineEdit_13.setText('')
            self.ui.lineEdit_14.setText('')

    @staticmethod
    def count_min_or_max_points(points):
        max_y, max_x, min_y, min_x = -5000, 0, 5000, 0
        for x, y in enumerate(points):
            if y > max_y:
                max_x = x
                max_y = y
            if y < min_y:
                min_x = x
                min_y = y
        return max_x, min_x

    def exit_prog(self):
        self.close()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    myapp = PyTunning()
    myapp.show()
    sys.exit(app.exec_())
